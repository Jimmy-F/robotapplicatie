//
// Created by jimmy on 21-9-16.
//

#include "Detector.hpp"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

Detector::Detector(const Shapes shape, const Colors color, const double scale) :
		shape(shape), color(color), scale(scale)
{
	std::pair<cv::Scalar, cv::Scalar> range = get_color_range(color);
	min_color = range.first;
	max_color = range.second;
}

S_Object Detector::find_white_circle(cv::Mat &img)
{
    cv::Mat image = img.clone();
    cv::Mat thresh = get_filtered_img(image, Colors::E_WHITE);
    cv::namedWindow("IMG");
    cv::imshow("IMG", thresh);
	std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::Mat dst = image;
    cv::findContours(thresh.clone(), contours, hierarchy, CV_RETR_LIST,
                     CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
    S_Object ret_obj;
    shape = E_CIRCLE;

    ret_obj = find_circle(dst, contours, hierarchy);

    return ret_obj;
}


std::vector<S_Object> Detector::find_shapes(cv::Mat &img) {
	cv::Mat image = img.clone();
	cv::Mat thresh = get_filtered_img(image, color);
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::Mat dst = image;

	cv::findContours(thresh.clone(), contours, hierarchy, CV_RETR_LIST,
			CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

    std::vector<S_Object> ret_objects;
    switch (shape)
    {
        case E_RECTANGLE:
            ret_objects = find_rect(dst, contours, hierarchy);
            break;
        case E_SQUARE:
            ret_objects = find_square(dst, contours, hierarchy);
            break;
        case E_CIRCLE:
            ret_objects.push_back(find_circle(dst, contours, hierarchy));
            break;
        case E_SEMI_CIRCLE:
            ret_objects = find_semi_circle(dst, contours, hierarchy);
            break;
        case E_TRIANGLE:
            ret_objects = find_triangle(dst, contours, hierarchy);
            break;
    }


	cv::namedWindow("Shapes", CV_WINDOW_AUTOSIZE);
	cv::imshow("Shapes", dst);
    return ret_objects;
}

std::pair<double, double> Detector::get_center(std::vector<cv::Point> &contour) {

	cv::Moments mu;
	mu = cv::moments(contour, false);
	cv::Point2d mc;
	mc = cv::Point2d(mu.m10 / mu.m00, mu.m01 / mu.m00);
	std::pair<double, double> coordinates { mc.x , mc.y  };
	return coordinates;

}

double Detector::get_surface(std::vector<cv::Point> &contour) {
	return cv::contourArea(contour)/(scale*scale);
}

cv::Mat Detector::get_filtered_img(cv::Mat &img, Colors _color) {

    std::pair<cv::Scalar, cv::Scalar> colorRange = get_color_range(_color);
	cv::Mat blurred_img;
	//cv::GaussianBlur(img, blurred_img, cv::Size(5, 5), 1.5, 1.5);
	//cv::medianBlur(img, blurred_img, 9);
	cv::cvtColor(img, blurred_img, cv::COLOR_BGR2Lab);

	cv::Mat thresh;

	cv::inRange(blurred_img, colorRange.first, colorRange.second, thresh);

	cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7,7),cv::Point(-1,-1));
	cv::namedWindow("Thresh",cv::WINDOW_AUTOSIZE);
	cv::imshow("Thresh",thresh);
	return thresh;
}

Detector::~Detector() {

}

std::vector<S_Object> Detector::find_square(cv::Mat &img,
                                            std::vector<std::vector<cv::Point>> contours,
                                            std::vector<cv::Vec4i> hierarchy) {

	std::vector<cv::Point> approx;
    std::vector<S_Object> ret_vector;

	for (unsigned int i = 0; i < contours.size(); i++) {
		// Approximate contour with accuracy proportional
		// to the contour perimeter
		cv::approxPolyDP(cv::Mat(contours[i]), approx,
				cv::arcLength(cv::Mat(contours[i]), true) * 0.02, true);

		// Skip small or non-convex objects
		if (std::fabs(cv::contourArea(contours[i])) < 100
				|| !cv::isContourConvex(approx))
			continue;

		if (approx.size() == 4) {
			cv::Rect2d r = cv::boundingRect(contours[i]);
			// Number of vertices of polygonal curve
			unsigned long vtc = approx.size();

			// Get the cosines of all corners
			std::vector<double> cos;
			for (unsigned long j = 2; j < vtc + 1; j++)
				cos.push_back(
						angle(approx[j % vtc], approx[j - 2], approx[j - 1]));

			// Sort ascending the cosine values
			std::sort(cos.begin(), cos.end());

			// Get the lowest and the highest cosine
			double mincos = cos.front();
			double maxcos = cos.back();

			// Use the degrees obtained above and the number of vertices
			// to determine the shape of the contour
			if (vtc == 4 && mincos >= -0.1 && maxcos <= 0.3
					&& shape == E_SQUARE
					&& std::abs(1 - (double) r.width / r.height) <= 0.2) {
				std::pair<double, double> c = get_center(contours[i]);
                S_Object foundObj;

                scale = r.width / 7.6;
				std::cout << " scale :" << r.width << std::endl;
                foundObj.id = i;
				foundObj.surface = (r.width/scale) * (r.height/scale);
                foundObj.point.x = c.first ;
                foundObj.point.y = c.second ;
                foundObj.scale = scale;
                ret_vector.push_back(foundObj);
                //std::cout << "scale: " << r.width / 7.6 << std::endl;

                set_label(img,
                          "(" + std::to_string((long) c.first) + ","
                          + std::to_string((long) c.second) + ")",
                          contours[i]);
				std::cout << "surface: " << (r.width/scale) * (r.height/scale)
						<< std::endl;
				cv::drawContours(img, contours, i, cv::Scalar(200, 200, 200), 2,
						8, hierarchy, 0);
			}
		}
	}
	return ret_vector;
}
 std::vector<S_Object> Detector::find_rect(cv::Mat &img,
                                           std::vector<std::vector<cv::Point>> contours,
                                           std::vector<cv::Vec4i> hierarchy) {

	 std::vector<S_Object> ret_objs;
	std::vector<cv::Point> approx;

	for (unsigned int i = 0; i < contours.size(); i++) {
		// Approximate contour with accuracy proportional
		// to the contour perimeter
		cv::approxPolyDP(cv::Mat(contours[i]), approx,
				cv::arcLength(cv::Mat(contours[i]), true) * 0.02, true);

		// Skip small or non-convex objects
		if (std::fabs(cv::contourArea(contours[i])) < 100
				|| !cv::isContourConvex(approx))
			continue;

		if (approx.size() == 4) {
			cv::Rect r = cv::boundingRect(contours[i]);
			// Number of vertices of polygonal curve
			long unsigned int vtc = approx.size();

			// Get the cosines of all corners
			std::vector<double> cos;
			for (long unsigned int j = 2; j < vtc + 1; j++)
				cos.push_back(
						angle(approx[j % vtc], approx[j - 2], approx[j - 1]));

			// Sort ascending the cosine values
			std::sort(cos.begin(), cos.end());

			// Get the lowest and the highest cosine
			double mincos = cos.front();
			double maxcos = cos.back();

			// Use the degrees obtained above and the number of vertices
			// to determine the shape of the contour

			if (vtc == 4 && mincos >= -0.1 && maxcos <= 0.3 &&
					 shape == E_RECTANGLE
					&& std::abs(1 - (double) r.width / r.height) >= 0.2) {
				std::pair<double, double> c = get_center(contours[i]);
				S_Object obj;
				obj.id = i;
				obj.point.x = c.first;
				obj.point.y = c.second;
				obj.surface = cv::contourArea(contours[i])/ (scale * scale);
				ret_objs.push_back(obj);
                set_label(img,
                          "(" + std::to_string((long) c.first) + ","
                          + std::to_string((long) c.second) + ")",
                          contours[i]);
				std::cout << "surface: " << cv::contourArea(contours[i])/ (scale * scale)
						<< std::endl;
				cv::drawContours(img, contours, i, cv::Scalar(200, 200, 200), 2,
						8, hierarchy, 0);
			}

		}
	}
	return ret_objs;
}
S_Object Detector::find_circle(cv::Mat &img,
                               std::vector<std::vector<cv::Point>> contours,
                               std::vector<cv::Vec4i> hierarchy) {

	std::vector<cv::Point> approx;
	S_Object ret_obj;
	ret_obj.id = NO_WHITE_CIRCLE;

	for (unsigned int i = 0; i < contours.size(); i++) {
		// Approximate contour with accuracy proportional
		// to the contour perimeter
		cv::approxPolyDP(cv::Mat(contours[i]), approx,
				cv::arcLength(cv::Mat(contours[i]), true) * 0.02, true);

		// Skip small or non-convex objects
		if (std::fabs(cv::contourArea(contours[i])) < 100
				|| !cv::isContourConvex(approx)) {
            continue;
        }
			// Detect and label circles
			double area = cv::contourArea(contours[i]);
			cv::Rect r = cv::boundingRect(contours[i]);

			int radius = r.width / 2;

			if (std::abs(1 - ((double) r.width / r.height)) <= 0.2
					&& std::abs(1 - (area / (CV_PI * std::pow(radius, 2))))
							<= 0.2 && shape == E_CIRCLE && std::abs(1 - (area / (r.width * r.height))) > 0.2 && approx.size() != 4) {
				cv::drawContours(img, contours, i, cv::Scalar(200, 200, 200), 2,
						8, hierarchy, 0);
				std::pair<double, double> c = get_center(contours[i]);
				S_Object obj;
				obj.id = i;
				obj.point.x = c.first;
				obj.point.y = c.second;
				obj.surface =  CV_PI * std::pow((r.width)/2, 2);
                std::cout << "WIDTH: " << r.width << std::endl;
				ret_obj = obj;
                set_label(img,
                          "(" + std::to_string((long) c.first) + ","
                          + std::to_string((long) c.second) + ")",
                          contours[i]);
				std::cout << "x: " << c.first << std::endl;
				std::cout << "y: " << c.second << std::endl;
				std::cout << "surface: " << CV_PI * std::pow((r.width/scale)/2, 2)
						<< std::endl;
			}
	}
	return ret_obj;
}
std::vector<S_Object> Detector::find_semi_circle(cv::Mat &img,
                                                 std::vector<std::vector<cv::Point>> contours,
                                                 std::vector<cv::Vec4i> hierarchy) {
	std::vector<S_Object> ret_objs;
	std::vector<cv::Point> approx;
	for (unsigned int i = 0; i < contours.size(); i++) {
		// Approximate contour with accuracy proportional
		// to the contour perimeter
		cv::approxPolyDP(cv::Mat(contours[i]), approx,
				cv::arcLength(cv::Mat(contours[i]), true) * 0.02, true);

		// Skip small or non-convex objects
		if (std::fabs(cv::contourArea(contours[i])) < 100
				|| !cv::isContourConvex(approx))
			continue;

		double area = cv::contourArea(contours[i]);
		cv::Rect r = cv::boundingRect(contours[i]);

		int radius = r.width;

		if (std::abs((double) r.width / r.height) >= 0.3
				&& std::abs(1 - (area / ((CV_PI * std::pow(radius, 2)) / 2)))
						<= 0.2) {
			cv::drawContours(img, contours, i, cv::Scalar(200, 200, 200), 2, 8,
					hierarchy, 0);
			std::pair<double, double> c = get_center(contours[i]);
			S_Object obj;
			obj.id = i;
			obj.point.x = c.first;
			obj.point.y = c.second;
			obj.surface = get_surface(contours[i]);
			ret_objs.push_back(obj);
            set_label(img,
                      "(" + std::to_string((long) c.first) + ","
                      + std::to_string((long) c.second) + ")",
                      contours[i]);
			std::cout << "x: " << c.first << std::endl;
			std::cout << "y: " << c.second << std::endl;
			std::cout << "surface: " << get_surface(contours[i]) << std::endl;
		}

	}
	return ret_objs;
}

std::vector<S_Object> Detector::find_triangle(cv::Mat &img,
                                              std::vector<std::vector<cv::Point>> contours,
                                              std::vector<cv::Vec4i> hierarchy) {
	std::vector<cv::Point> approx;
	std::vector<S_Object> ret_objs;
	for (unsigned int i = 0; i < contours.size(); i++) {
		// Approximate contour with accuracy proportional
		// to the contour perimeter
		cv::approxPolyDP(cv::Mat(contours[i]), approx,
				cv::arcLength(cv::Mat(contours[i]), true) * 0.02, true);

		// Skip small or non-convex objects
		if (std::fabs(cv::contourArea(contours[i])) < 100
				|| !cv::isContourConvex(approx))
			continue;

		if (approx.size() == 3) {
			cv::drawContours(img, contours, i, cv::Scalar(200, 200, 200), 2, 8,
					hierarchy, 0);
			std::pair<double, double> c = get_center(contours[i]);
			S_Object obj;
			obj.id = i;
			obj.point.x = c.first;
			obj.point.y = c.second;
			obj.surface = get_surface(contours[i]);
			ret_objs.push_back(obj);
            set_label(img,
                      "(" + std::to_string((long) c.first) + ","
                      + std::to_string((long) c.second) + ")",
                      contours[i]);
			std::cout << "x: " << c.first << std::endl;
			std::cout << "y: " << c.second << std::endl;
			std::cout << "surface: " << get_surface(contours[i]) << std::endl;
		}
	}
	return ret_objs;
}

void Detector::set_label(cv::Mat &im, const std::string label,
                         std::vector<cv::Point> &contour) {
	int fontface = cv::FONT_HERSHEY_SIMPLEX;
	double scale = 0.3;
	int thickness = 1;
	int baseline = 0;

	cv::Size text = cv::getTextSize(label, fontface, scale, thickness,
			&baseline);
	cv::Rect r = cv::boundingRect(contour);

	cv::Point pt(r.x + ((r.width - text.width) / 2),
			r.y + ((r.height + text.height) / 2));
	cv::rectangle(im, pt + cv::Point(0, baseline),
			pt + cv::Point(text.width, -text.height), CV_RGB(255, 255, 255),
			CV_FILLED);
	cv::putText(im, label, pt, fontface, scale, CV_RGB(0, 0, 0), thickness, 8);
}

double Detector::angle(cv::Point pt1, cv::Point pt2, cv::Point pt0) {
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;
	return (dx1 * dx2 + dy1 * dy2)
			/ sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);
}

std::pair<cv::Scalar, cv::Scalar> Detector::get_color_range(Colors color) {
	std::pair<cv::Scalar, cv::Scalar> colorRange { cv::Scalar(0, 0, 0),	cv::Scalar(0, 0, 0) };

    switch (color)
    {
        case E_RED:
            colorRange.first = cv::Scalar(50, 173, 139);
            colorRange.second = cv::Scalar(238, 215, 255);
            break;
        case E_GREEN:
            colorRange.first = cv::Scalar(191, 55, 170);
            colorRange.second = cv::Scalar(222, 93, 218);
            break;
        case E_BLUE:
            colorRange.first = cv::Scalar(106, 101, 84);
            colorRange.second = cv::Scalar(190, 119, 124);
            break;
        case E_YELLOW:
            colorRange.first = cv::Scalar(207, 86, 191);
            colorRange.second = cv::Scalar(255, 112, 215);
            break;
        case E_BLACK:
            colorRange.first = cv::Scalar(0, 117, 116);
            colorRange.second = cv::Scalar(90, 169, 199);
            break;
        case E_WHITE:
            colorRange.first = cv::Scalar(168, 117, 116);
            colorRange.second = cv::Scalar(255, 255, 146);
            break;
        case E_PINK:
            colorRange.first = cv::Scalar(106, 161, 122);
            colorRange.second = cv::Scalar(255, 229, 152);
            break;
        case E_ORANGE:
            colorRange.first = cv::Scalar(150, 141, 179);
            colorRange.second = cv::Scalar(255, 202, 229);
            break;
		case E_WOOD:
			colorRange.first = cv::Scalar(0,0,0);
			colorRange.second = cv::Scalar(255,255,132);
			break;
    }

	return colorRange;
}

double Detector::get_scale() const {
    return scale;
}

void Detector::set_shape(Shapes shape) {
	Detector::shape = shape;
}

void Detector::set_scale(double scale) {
	Detector::scale = scale;
}

void Detector::set_color(Colors color) {
	Detector::color = color;
}

