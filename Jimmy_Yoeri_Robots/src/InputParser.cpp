/*
 * InputParser.cpp
 *
 *  Created on: Sep 21, 2016
 *      Author: chris
 */

#include "InputParser.hpp"
#include "Detector.hpp"
#include "Config.hpp"


InputParser::InputParser() :
		closeProgram(false), parsingComplete(false), color(""), shape(""),parsedInput()  {
	image = takePicture();
	initColorList();
	initShapeList();
}

InputParser::~InputParser() {
}

S_Object InputParser::parseInput(std::stringstream& input) {
	std::string inputShape;
	S_Object object;
	input >> inputShape;
	checkForWordExit(inputShape);
	for(unsigned char i = 0; i < shapeList.size(); i++) {
		if(inputShape == shapeList[i]) {
			if(shapeList[i] == "halve") {
				checkForWordHalveCircel(input);
			}
			else {
				shape = shapeList[i];
				std::cout << shape << " ";
				object = searchForColor(input);
			}
		}
	}
	return object;
}

S_Object InputParser::searchForColor(std::stringstream& input) {
	std::string inputColor;
	S_Object object;
	input >> inputColor;
	for(unsigned char i = 0; i < colorList.size(); i++) {
		if(inputColor == colorList[i]) {
			color = colorList[i];
			std::cout << color << std::endl;
            object = createObject();
		}
	}
	return object;
}

void InputParser::checkForWordHalveCircel(std::stringstream& input) {
	std::string inputShape;
	input >> inputShape;
	if(inputShape == "circel") {
		shape = "halve circel";
		std::cout << shape << std::endl;
		searchForColor(input);
	}
}

bool InputParser::getCloseProgram() {
	return closeProgram;
}

void InputParser::initColorList() {
	colorList.push_back("wit");
	colorList.push_back("zwart");
	colorList.push_back("rood");
	colorList.push_back("groen");
	colorList.push_back("blauw");
	colorList.push_back("geel");
    colorList.push_back("roze");
    colorList.push_back("oranje");
	colorList.push_back("hout");
}

void InputParser::checkForWordExit(std::string input) {
	if(input == "exit") {
		closeProgram = true;
	}
}

void InputParser::initShapeList() {
	shapeList.push_back("circel");
	shapeList.push_back("vierkant");
	shapeList.push_back("driehoek");
	shapeList.push_back("rechthoek");
	shapeList.push_back("halve");
}

cv::Mat InputParser::takePicture() {
//	cv::VideoCapture cap(1);
	cv::Mat frame = cv::imread("../Images/plaatjes.png");
	return frame;
//	if(!cap.isOpened()) {
//		std::cout << "Webcam not found. No picture taken." << std::endl;
//		frame = cv::imread("../pictures/Foto.jpg",1);
//		return frame;
//	}
//	else {
//		cap >> frame;
//		cap.release();
//		return frame;
//	}
}

S_Object InputParser::createObject()
{
    S_Object ret;

    if (color == "rood") {
        ret.color = E_RED;
    } else if (color == "wit"){
        ret.color = E_WHITE;
    } else if (color == "geel"){
        ret.color = E_YELLOW;
    } else if (color == "roze"){
        ret.color = E_PINK;
    } else if (color == "oranje"){
        ret.color = E_ORANGE;
    } else if (color == "zwart"){
        ret.color = E_BLACK;
    } else if (color == "blauw"){
        ret.color = E_BLUE;
    } else if (color == "groen") {
		ret.color = E_GREEN;
	} else if (color == "hout")	{
		ret.color = E_WOOD;
	}

    if (shape == "vierkant") {
        ret.shape = E_SQUARE;
    } else if (shape == "rechthoek") {
        ret.shape = E_RECTANGLE;
    } else if (shape == "circel") {
        ret.shape = E_CIRCLE;
    } else if (shape == "halve") {
        ret.shape = E_SEMI_CIRCLE;
    } else if (shape == "driehoek") {
        ret.shape = E_TRIANGLE;
    }

    return ret;
}