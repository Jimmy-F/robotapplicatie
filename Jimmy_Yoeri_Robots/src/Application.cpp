//
// Created by yoeri on 3/28/18.
//

#include "Application.hpp"
#include "Config.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <vector>

Application::Application() : m_serial(ROBOT_PORT, BAUDRATE)
{
}

Application::~Application() {

}

bool Application::start() {
    goto_start_position();
    process();
    return true;
}

void Application::goto_start_position() {
    std::vector<double> park_angles = m_cmd.get_cmdlist_straight_up_deg();
    std::vector<std::string> park_names = m_cmd.get_cmdlist_straight_up_name();

    m_serial.write_string(m_cmd.map_angle_to_pulse(park_angles, park_names));
    m_serial.write_string(m_cmd.open_gripper(true));
}

bool Application::take_image(cv::Mat &image)
{
    cv::VideoCapture cap;
    if(!cap.open(VIDEO_CAMERA)) {
        throw std::runtime_error(std::string("Can't open camera."));
    }
    cap >> image;
    cap.release();
    return true;
}


bool Application::process() {
    while (m_inputParser.getCloseProgram() == false) {
        if (m_inputParser.getCloseProgram()) {
            return 0;
        }
        std::cout << "Input format: " << std::endl;
        std::cout << "<shape> <input>" << std::endl;

        std::string input;
        std::getline(std::cin, input);
        std::stringstream inputString(input);
        S_Object toFind;
        toFind = m_inputParser.parseInput(inputString);

        Detector d(toFind.shape, toFind.color, 1);

        Kinematics IKobj("Shapes", d.get_scale());

        cv::Mat image;

        take_image(image);

        S_Object scaleObj;
        scaleObj.color = E_GREEN;
        scaleObj.shape = E_SQUARE;

        std::vector<S_Object> objects;

        d.set_color(scaleObj.color);
        d.set_shape(scaleObj.shape);

        objects = d.find_shapes(image);

        if(objects.size() == 0) {
            std::cout << "Scale reference object not found, please insert the GREEN SQUARE object." << std::endl;
            process();
            return 1;
        }
        else
        {
            scaleObj.scale = objects[0].scale;
        }

        d.set_shape(toFind.shape);
        d.set_color(toFind.color);
        objects = d.find_shapes(image);
        for (unsigned int i = 0; i < objects.size(); ++i)
        {
            std::cout << "ID: " << objects[i].id << std::endl;
            std::cout << "surface: " << objects[i].surface << std::endl;
            std::cout << "Point: x: " <<  objects[i].point.x << " y: " << objects[i].point.y << std::endl;
            std::cout << "Scale: " <<  objects[i].scale << std::endl;
        }

        if(objects.size() == 0)
        {
            std::cout << "No objects found, please input another object." << std::endl;
            process();
            return 1;
        }

        S_Object white_circle;
        white_circle = d.find_white_circle(image);

        if(white_circle.id == -1)
        {
            throw std::runtime_error(std::string("Goal not found exiting!."));
        }

        int n = 0;
        IKobj.set_robot_base();


        while((char)cv::waitKey(1) != 27)
        {
            if (Kinematics::is_robot_base_set() == true)
            {
                IKobj.set_scale(scaleObj.scale);
                std::cout << "Angle: " << IKobj.get_angle_base_to_obj(objects[n]) << std::endl;
                std::vector<double> angles = {IKobj.get_angle_base_to_obj(objects[n])};
                std::vector<std::string> joints = {"Base"};
                m_serial.write_string(m_cmd.map_angle_to_pulse(angles, joints));
                sleep(3);
                break;
            }
        }
        IKobj.initialise();

        std::vector<double> angles;
        std::vector<std::string> SW_joints = {"Shoulder", "Wrist"};

        std::cout << "Finding IK for target" << std::endl;
        std::cout << "-----------------------------------" << std::endl;

        angles = IKobj.inverse_kinematics();

        std::vector<double> SW_angles;
        SW_angles.push_back(round(angles[0]));
        SW_angles.push_back(round(angles[2]));

        m_serial.write_string(m_cmd.map_angle_to_pulse(SW_angles, SW_joints));
        sleep(1);

        std::vector<double> E_angle;
        std::vector<std::string> E_joint = {"Elbow"};
        E_angle.push_back(angles[1]);

        m_serial.write_string(m_cmd.map_angle_to_pulse(E_angle, E_joint));
        sleep(1);
        m_serial.write_string(m_cmd.open_gripper(false));
        sleep(3);
        std::vector<double> park_angles = {0, -30, 120, 90};
        std::vector<std::string> park_joints {"Base", "Shoulder", "Elbow", "Wrist"};
        m_serial.write_string(m_cmd.map_angle_to_pulse(park_angles, park_joints));
        sleep(3);


        std::vector<double> angles_wc = {IKobj.get_angle_base_to_obj(white_circle)};
        std::vector<std::string> joints_b = {"Base"};
        m_serial.write_string(m_cmd.map_angle_to_pulse(angles_wc, joints_b));

        std::cout << "Inverse kinematics for white circle" << std::endl;
        std::cout << "-----------------------------------" << std::endl;

        angles = IKobj.inverse_kinematics();
        std::vector<std::string> le_joints = {"Shoulder" , "Elbow" , "Wrist"};
        sleep(3);

        m_serial.write_string(m_cmd.map_angle_to_pulse(angles, le_joints));
        sleep(1);
        m_serial.write_string(m_cmd.open_gripper(true));

        sleep(3);
        m_serial.write_string(m_cmd.map_angle_to_pulse(park_angles, park_joints));


        cv::destroyAllWindows();

        goto_start_position();
    }
    return 1;
}
