//
// Created by yoeri on 2/27/18.
//

#ifndef JIMMY_YOERI_ROBOTS_CONFIG_HPP
#define JIMMY_YOERI_ROBOTS_CONFIG_HPP

#define ROBOT_PORT "/dev/ttyUSB0"
#define VIDEO_CAMERA 0
#define BAUDRATE 9600

#define NO_WHITE_CIRCLE -1


#define VELOCITY 500
#define DURATIONLENGTH 1500


// Robot part lengths
#define L_LOWER_ARM  14.605
#define L_UPPER_ARM  18.7325
#define L_WRIST  8.5725


typedef enum {
    E_RED,
    E_GREEN,
    E_BLUE,
    E_YELLOW,
    E_PINK,
    E_ORANGE,
    E_BLACK,
    E_WHITE,
    E_WOOD
} Colors;

typedef enum {
    E_SQUARE,
    E_RECTANGLE,
    E_CIRCLE,
    E_SEMI_CIRCLE,
    E_TRIANGLE
} Shapes;

struct S_Point {
    double x;
    double y;
};

struct S_Object{
    Colors color;
    Shapes shape;
    S_Point point;
    int id;
    double surface;
    double scale;
};



#endif //JIMMY_YOERI_ROBOTS_CONFIG_HPP
