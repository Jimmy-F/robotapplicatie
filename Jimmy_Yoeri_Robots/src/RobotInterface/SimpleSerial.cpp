/*
 * SimpleSerial.cpp
 *
 *      Author: Jimmy Feltsadas & Yoeri Smit
 */


#ifndef SRC_simple_serial_CPP_
#define SRC_simple_serial_CPP_

#include "../RobotInterface/SimpleSerial.hpp"

#include <iostream>

SimpleSerial::SimpleSerial(const std::string& port, const unsigned long int baud_rate) :
		m_io(), m_serial(m_io, port) {

	m_serial.set_option(boost::asio::serial_port_base::baud_rate((int)baud_rate));
}

void SimpleSerial::write_string(std::string aString) {
	//std::cout << "COMMAND: " << aString << std::endl;
	boost::asio::write(m_serial, boost::asio::buffer(aString.c_str(), aString.size()));
}

SimpleSerial::~SimpleSerial() {

}

#endif /* SRC_simple_serial_CPP_ */
