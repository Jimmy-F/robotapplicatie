/*
 * SerialCommand.hpp
 *
 *      Author: Jimmy Feltsadas & Yoeri Smit
 */

#ifndef SERIALCOMMAND_HPP_
#define SERIALCOMMAND_HPP_

#include <string>
#include <vector>
#include <tuple>
#include <sstream>
#include <utility>
#include <cmath>
#include <map>

/**
 * @class This function converts the values of then new angles and the associated joint names to a command line which can be send
 * to the robotic arm.
 */
class SerialCommand {
public:
	
	SerialCommand();
	
	virtual ~SerialCommand();

	/**
	 * @brief Maps the given angle to a pulseWidth.
	 * @param string containing a angle.
	 * @param tuple containing the anglewidth of a part.
	 * @return string containing the pulsWidth.
	 */
	std::string map_angle_to_pulse(const std::vector<double>& angles, const std::vector<std::string>& joints);

    std::string open_gripper(const bool open);

	std::vector<std::string> get_cmdlist_straight_up_name();
	std::vector<std::string> get_cmdlist_park_name();
	std::vector<double> get_cmdlist_straight_up_deg();
	std::vector<double> get_cmdlist_park_deg();

private:
	
	/**
	 * @brief This function builds a command line based on a pair vector which holds the combination of a joint and pulsewidth
	 * of multiple joints..
	 * @param newPos The vector which holds a pair which holds a joint name and a pulsewidth.
	 * @return It returns the complete command line string which can be send to the robotic arm.
	 */
	std::string build_cmdline(const std::vector<std::pair<std::string, std::string>>& new_pos);

	/** mapAngleToPulse(
	 * @brief Map function
	 * @param int containing the original value.
	 * @param int containing the in minimal value.
	 * @param int containing the in maximum value.
	 * @param int containing the out minimal value.
	 * @param int containing the out maximum value.
	 * @return int containing the mapped value.
	 */
	int map(const int x, const int in_min, const int in_max, const int out_min, const int out_max);

	/**
	 * @brief Initializes the partsList vector.
	 */
	void init_parts_list();
	
	/**
	 * @brief Initializes the commandsList vector.
	 */
	void init_cmdlist();
	
	/**
	 * 
	 */
	std::vector<std::string> m_cmdlist_straight_up_name;

	/**
	 *
	 */
	std::vector<double> m_cmdlist_straight_up_deg;

	/**
	 *
	 */
	std::vector<std::string> m_cmdlist_park_name;

	/**
	 *
	 */
	std::vector<double> m_cmdlist_park_deg;
	
	/**
	 * 
	 */
	std::vector<std::tuple<std::string, std::string, int, int, int, int>> m_parts_list;

};

#endif /* SERIALCOMMAND_HPP_ */
