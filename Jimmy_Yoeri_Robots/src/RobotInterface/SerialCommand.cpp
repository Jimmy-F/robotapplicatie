/*
 * SerialCommand.cpp
 *
 *      Author: Jimmy Feltsadas & Yoeri Smitl
 */

#include "../RobotInterface/SerialCommand.hpp"

#include <iostream>

#include "../Config.hpp"

SerialCommand::SerialCommand(){
	init_parts_list();
	init_cmdlist();
}

SerialCommand::~SerialCommand() {
}

std::string SerialCommand::map_angle_to_pulse(const std::vector<double>& angles, const std::vector<std::string>& joints) {
    if (angles.size() == joints.size()){
		std::vector<std::pair<std::string, std::string>> new_conf;
		std::vector<int> pulse_angles;
		for (size_t j = 0; j < angles.size(); ++j){
         //   std::cout << "ANGLE[" << j << "]: " << angles.at(j) << std::endl;
			std::tuple<std::string, std::string, int, int, int, int> parts_list;
			bool joint_found = false;
			for (size_t i = 0; i < m_parts_list.size(); ++i){
				if (joints.at(j) == std::get < 0 > (m_parts_list.at(i))){
					parts_list = m_parts_list.at(i);
					joint_found = true;
					break;
				}
			}
			if (joint_found) {
				pulse_angles.push_back(map((int)std::round(angles.at(j)), std::get < 2 > (parts_list), std::get < 3 > (parts_list), std::get < 4 > (parts_list), std::get < 5 > (parts_list)));
				std::stringstream angle_ss;
				angle_ss << pulse_angles.at(j);
				std::string pulse_str = "P" + angle_ss.str();
				std::pair<std::string, std::string> aPair (joints.at(j), pulse_str);
				new_conf.push_back(aPair);
			}
		}
		return build_cmdline(new_conf);
	}
	return "";
}

int SerialCommand::map(const int x, const int in_min, const int in_max, const int out_min, const int out_max) {
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

std::string SerialCommand::build_cmdline(const std::vector<std::pair<std::string, std::string>>& new_pos){
	std::string cmd = "";
	for (unsigned char i = 0; i < new_pos.size(); ++i) {
		for(unsigned char j = 0; j < m_parts_list.size(); ++j){
			if (std::get < 0 > (m_parts_list[j]) == new_pos.at(i).first){
				cmd = cmd + std::get < 1 > (m_parts_list[j]) + new_pos.at(i).second;
				break;
			}
		}
	}
	cmd += "S" + std::to_string(VELOCITY) + "T" + std::to_string(DURATIONLENGTH) + '\r';
	return cmd;
}

void SerialCommand::init_parts_list() {
	m_parts_list.push_back(std::make_tuple("Base", "#0", -90, 90, 580, 2400));
	m_parts_list.push_back(std::make_tuple("Shoulder", "#1", -30, 90, 1750, 560));
	m_parts_list.push_back(std::make_tuple("Elbow", "#2", 0, 135, 300, 1880));
	m_parts_list.push_back(std::make_tuple("Wrist", "#3", -90, 90, 2150, 700));
	m_parts_list.push_back(std::make_tuple("Gripper", "#5", -90, 90, 400, 2100));
	m_parts_list.push_back(std::make_tuple("Wristrotate", "#4", -90, 90, 2500, 660));
}


void SerialCommand::init_cmdlist() {
	m_cmdlist_straight_up_name.push_back("Base");
	m_cmdlist_straight_up_name.push_back("Shoulder");
	m_cmdlist_straight_up_name.push_back("Elbow");
	m_cmdlist_straight_up_name.push_back("Wrist");
	m_cmdlist_straight_up_name.push_back("Gripper");
	m_cmdlist_straight_up_name.push_back("Wristrotate");

    m_cmdlist_straight_up_deg.push_back(90);
	m_cmdlist_straight_up_deg.push_back(-30);
	m_cmdlist_straight_up_deg.push_back(120);
	m_cmdlist_straight_up_deg.push_back(0);
	m_cmdlist_straight_up_deg.push_back(0);
	m_cmdlist_straight_up_deg.push_back(0);

	m_cmdlist_park_name.push_back("Base");
	m_cmdlist_park_name.push_back("Shoulder");
	m_cmdlist_park_name.push_back("Elbow");
	m_cmdlist_park_name.push_back("Wrist");
	m_cmdlist_park_name.push_back("Gripper");
	m_cmdlist_park_name.push_back("Wristrotate");

	m_cmdlist_park_deg.push_back(90);
	m_cmdlist_park_deg.push_back(-30);
	m_cmdlist_park_deg.push_back(0);
	m_cmdlist_park_deg.push_back(0);
	m_cmdlist_park_deg.push_back(0);
	m_cmdlist_park_deg.push_back(0);

}

std::vector<std::string> SerialCommand::get_cmdlist_straight_up_name() {
	return m_cmdlist_straight_up_name;
}

std::vector<std::string> SerialCommand::get_cmdlist_park_name() {
	return m_cmdlist_park_name;
}

std::vector<double> SerialCommand::get_cmdlist_straight_up_deg() {
	return m_cmdlist_straight_up_deg;
}

std::vector<double> SerialCommand::get_cmdlist_park_deg() {
	return m_cmdlist_park_deg;
}

std::string SerialCommand::open_gripper(const bool open){
    std::string gripper_cmd;
    for (size_t i = 0; i < m_parts_list.size(); ++i) {
        if (std::get<0>(m_parts_list[i]) == "Gripper") {
            gripper_cmd = std::get<1>(m_parts_list[i]);
            gripper_cmd += 'P';
            if (open) {
                gripper_cmd += std::to_string(std::get<4>(m_parts_list[i]));
            } else {
                gripper_cmd += std::to_string(std::get<5>(m_parts_list[i]));
            }
            gripper_cmd += 'S' + std::to_string(VELOCITY) + 'T' + std::to_string(DURATIONLENGTH) + '\r';
            break;
        }
    }
    return gripper_cmd;
}
