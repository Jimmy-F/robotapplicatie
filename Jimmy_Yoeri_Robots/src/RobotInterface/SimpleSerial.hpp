/*
 * SimpleSerial.hpp
 *
 *      Author: Jimmy Feltsadas & Yoeri Smit
 */

#ifndef SRC_simple_serial_HPP_
#define SRC_simple_serial_HPP_

#include <boost/asio.hpp>

/**
 * @class This class enables the serial connection with the AL5D robotic arm.
 */
class SimpleSerial {
public:
	/**
	 * @brief Constructor.
	 * @param port device name, example "/dev/ttyUSB0" or "COM4".
	 * @param baud_rate communication speed, example 9600 or 115200.
	 * @throws boost::system::system_error if cannot open the serial device.
	 */
	SimpleSerial(const std::string& port, const unsigned long baud_rate);

	/**
	 * @brief Write a string to the serial device.
	 * @param s string to write.
	 * @throws boost::system::system_error on failure.
	 */
	void write_string(std::string str);

	/**
	 * @brief Blocks until a line is received from the serial device.
	 * Eventual '\n' or '\r\n' characters at the end of the string are removed.
	 * @return a string containing the received line.
	 * @throws boost::system::system_error on failure.
	 */
	std::string read_line();

	/**
	 * @brief Destructor
	 */
	virtual~SimpleSerial();

private:
	/**
	 * @brief The input and output object which is required for the serial interface.
	 */
	boost::asio::io_service m_io;

	/**
	 * @brief The serial interface object.
	 */
	boost::asio::serial_port m_serial;
};

#endif /* SRC_simple_serial_HPP_ */
