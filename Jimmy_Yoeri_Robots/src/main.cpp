//============================================================================
// Name        : RobotApp.cpp
// Author      : Jimmy Feltsadas
// Version     : 0.1
// Copyright   : MIT
// Description : RobotApp
//============================================================================

#include "Application.hpp"

int main(int argc, char* argv[])
{
    (void)argc;
    (void)argv;

    try {
        Application app;
        if(app.start())
        {
            std::cout << "exit" <<std::endl;
            std::exit(EXIT_SUCCESS);
        }
    }
    catch (const std::exception& e) {
        std::cout << "Exception caught: " << e.what() << "'\n" << std::endl;
    }


}