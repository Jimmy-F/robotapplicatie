//
// Created by yoeri on 3/28/18.
//

#ifndef JIMMY_YOERI_ROBOTS_APPLICATION_HPP
#define JIMMY_YOERI_ROBOTS_APPLICATION_HPP

#include "RobotInterface/SimpleSerial.hpp"
#include "RobotInterface/SerialCommand.hpp"
#include "InputParser.hpp"
#include "Detector.hpp"
#include "Kinematics.hpp"


class Application {
public:
    Application();

    virtual ~Application();


public:

    bool start();

private:
    /**
     * Sets the robot to the start position
     */
    void goto_start_position();
    bool process();
    /**
     * Takes an image from the camera and set it to the image
     * @param image Sets the picture taken by the camera to this matrix
     * @return A boolean if the picture has been taken successfully/
     */
    bool take_image(cv::Mat &image);

    /**
     * SimpleSerial class used for serial communication with the robot
     */
    SimpleSerial m_serial;

    /**
     * SerialCommand class that converts angles and joints to commands for the serial
     */
    SerialCommand m_cmd;

    /**
     * InputParser used for parsing user input.
     */
    InputParser m_inputParser;




};


#endif //JIMMY_YOERI_ROBOTS_APPLICATION_HPP
