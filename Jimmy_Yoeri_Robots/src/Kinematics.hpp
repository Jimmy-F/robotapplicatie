//
// Created by jimmy on 17-3-18.
//

#ifndef JIMMY_YOERI_ROBOTS_IK_H
#define JIMMY_YOERI_ROBOTS_IK_H


#include <ostream>
#include <opencv2/core.hpp>
#include "Config.hpp"
#include <string>
#include <vector>

typedef enum {
    base_shoulder = 0,
    upper_lowerarm,
    lowerarm_wrist
} angle_names;

/**
 * Kinematics class used for calculating inverse kinematics and everything
 * connected to it. This means angle calculations and distance calculcations.
 */
class Kinematics {
public:
    /**
     * Constructor.
     * @param window_name Name of the window
     * @param scale Scale used for real world tranformation
     */
    Kinematics(const std::string& window_name, const double scale);

    /**
     * Destructor.
     */
    virtual ~Kinematics();

    /**
     * Initialises the scale and configuration used by inverse kinematics.
     */
    void initialise();

    /**
     * This function calculates the angles that need to be used
     * to get the robot to a target.
     *
     * @return a vector with angles for the robot
     */
    std::vector<double> inverse_kinematics();

    /**
     * Calculates a coordinate based on angles, XY coords and lengths.
     * Forward kinematics.
     * @param x0 Starting position X coord
     * @param y0 Starting position Y coord
     * @param theta1 First angle
     * @param l1 Length of the first part of the arm
     * @param theta2 Second angle
     * @param l2 Length of the second part of the arm
     * @param theta3 Third angle
     * @param l3 Length of the third part of the arm
     * @param deg Boolean to define if the given angles are degrees or radians
     * @return A coordinate based on the given parameters
     */
    S_Point forward_kinematics(double x0, double y0, double theta1, double l1, double theta2, double l2, double theta3, double l3, bool deg = true);

    /**
     * Function to be able to set the robot base coordinates.
     */
    void set_robot_base();

    /**
     * Calculates the angle from the base to a given object.
     * This function also sets the object as a Target used for inverse kinematics.
     * @param obj The object to calculate the angle from and set as target.
     * @return The angle between the robot base and the object.
     */
    double get_angle_base_to_obj(S_Object obj);

    /**
     *
     * @param event
     * @param x
     * @param y
     * @param flags
     * @param param
     */
    static void onMouseStatic(int event, int x, int y, int flags, void* param);

    /**
     * Getter for robot base.
     * @return The coordinates of the robot base.
     */
    static const cv::Point get_robot_base();

    /**
     * Getter for the robot position/
     * @return The angles of the current robot position.
     */
    const std::vector<int> &get_robot_pos() const;

    /**
     * Getter for checking if the robot base is set.
     * @return robot_base_set
     */
    static bool is_robot_base_set();

    /**
     * Getter for the length of the lower arm
     * @return r_lower_arm
     */
    double get_lower_arm() const;

    /**
     * Getter for the length of the upper arm.
     * @return r_upper_arm
     */
    double get_upper_arm() const;

    /**
     * Getter for the length of the wrist
     * @return r_wrist
     */
    double get_wrist() const;

    /**
     * Returns configuration anglses
     * @return configuration
     */
    const std::vector<double> &get_configuration() const;

    /**
     * Setter for the scale
     * @param scale
     */
    void set_scale(double scale);



private:
    /**
     * Computes the Jacobian matrix from a configuration.
     * @param configuration The current configuration
     * @return a jacobian matrix
     */
    std::vector<std::vector<double>> compute_jacobian(const std::vector<double> &configuration);

    /**
     * Calculates the delta config
     * @param goal
     * @param current_pose
     * @return vector containing the delta of the configuration
     */
    std::vector<double> calculate_delta_config(const S_Point &goal, const S_Point &current_pose);

    /**
     * Calculates new angles based on the old configuration and the delta theta
     * @param old_conf
     * @param delta_theta
     * @return vector of new angles
     */
    std::vector<double> calculate_new_angles(const std::vector<double> &old_conf,
                                             const std::vector<double> &delta_theta);

    /**
     * Checks if the goal is in range
     * @param e The end effector of the robot, based on forward kinematics
     * @param T The target coordinates
     * @param margin The margin that may goal may be in range of the goal
     * @return true if the goal is in range based on the margin
     */
    bool goal_in_range(S_Point e, S_Point T, double margin);

    /**
     * Converts radians to degrees
     * @param radians
     * @return vector of degrees
     */
    std::vector<double> rad_to_deg(const std::vector<double>& radians);

    /// / Callback function to set the cv::Point of the robot base.
    void onMouse(int event, int x, int y);

    /**
     * Scales the robot lengths
     * @param scale
     */
    void scale_robot_dimensions(double scale);

    /**
     * Converts a vector of degrees to radians
     * @param degrees
     * @return a vector of radians
     */
    std::vector<double> deg_to_rad(const std::vector<double>& degrees);

    // Coordinates of the base of the robot
    static cv::Point2d robot_base;
    // Boolean used for checking if the coordinates of the base are set.
    static bool robot_base_Set;

    // Name of the window.
    std::string name;

    //Angle in degrees which the robot is in.
    std::vector<int> robot_pos;

    // Robot dimensions
    double r_lower_arm;
    double r_upper_arm;
    double r_wrist;

    // Scale for transforming real world dimensions
    double scale;

    // Distance to the target
    double T_dist;

    // Stepsize for inverse kinematics
    double beta;

    // Robot angle conf
    std::vector<double> configuration;
    // Robot joint names
    std::vector<std::string> joints;

};


#endif //JIMMY_YOERI_ROBOTS_IK_H
