/*
 * Matrix.cpp
 *
 *  Created on: Mar 18, 2018
 *      Author: Jimmy Feltsadas & Yoeri Smit
 */

#include <iostream>
#include "Matrix.inl"
#include <math.h>       /* round, floor, ceil, trunc */
#include <stdlib.h>     /* abs */

namespace std {

    template <class T>
    Matrix<T>::Matrix(const vector<vector<T>>& aVec) : vec(aVec){
    }

    template <class T>
    Matrix<T>::~Matrix() {
    }

    template <class T>
    Matrix<T> Matrix<T>::operator+(const Matrix<T> &mat){
        vector<vector<T>> new_vec = this->get_vec();

        for (unsigned int i = 0; i < this->get_vec().size(); ++i){
            for(unsigned int j = 0; j < this->get_vec()[i].size(); ++j) {

                new_vec[i][j] = this->get_vec()[i][j] + mat.get_vec()[i][j];
                std::cout << new_vec[i][j];
            }
            std::cout << std::endl;
        }
        Matrix<T> new_mat(new_vec);
        return new_mat;
    }

    template <class T>
    Matrix<T> Matrix<T>::operator-(const Matrix<T> &mat){
        vector<vector<T>> new_vec = this->get_vec();

        for (unsigned int i = 0; i < this->get_vec().size(); ++i){
            for(unsigned int j = 0; j < this->get_vec()[i].size(); ++j) {
                new_vec[i][j] = this->get_vec()[i][j] - mat.get_vec()[i][j];
            }
        }
        Matrix<T> new_mat(new_vec);
        return new_mat;
    }

    template <class T>
    Matrix<T> Matrix<T>::operator*(const Matrix<T> &mat){
        vector<vector<T>> new_vec;
        new_vec.resize(this->get_vec().size());
        for (unsigned int i = 0; i < this->get_vec().size(); ++i){
            for (unsigned int j = 0; j < mat.get_vec()[0].size(); ++j){
                T x = 0;
                for (unsigned int k = 0; k < mat.get_vec().size(); ++k){
                    T multiplyer = this->get_vec()[i][k];
                    x += multiplyer * mat.get_vec()[k][j];

                }
                new_vec[i].push_back(x);
            }
        }
        Matrix<T> new_mat(new_vec);
        return new_mat;
    }

    template <class T>
    void Matrix<T>::transpose(){
        vector<vector<T>> new_vec(get_vec()[0].size(), std::vector<T> (get_vec().size()));
        for(unsigned int i = 0; i < get_vec().size(); ++i){
            for(unsigned int j = 0; j < get_vec()[i].size(); ++j){
                new_vec[j][i] = get_vec()[i][j];
            }
        }
        vec = new_vec;
    }

    template <class T>
    bool Matrix<T>::inverse_three_tatrix(){
        vector<vector<T>> temp = get_vec();
        if (get_vec().size() == 3 && get_vec()[0].size() == 3){

            double determinant = (vec[0][0] * vec[1][1] * vec[2][2]) + (vec[0][1] * vec[1][2] * vec[2][0]) +
                                 (vec[0][2] * vec[1][0]	* vec[2][1]) - (vec[0][2] * vec[1][1] * vec[2][0]) -
                                 (vec[0][1] * vec[1][0] * vec[2][2]) - (vec[0][0] * vec[1][2] * vec[2][1]);
            if (determinant == 0){
                cout << "Det = 0" << endl;
                return false;
            }
            double invertVec = 1 / determinant;
            vec[0][0] = (temp[1][1] * temp[2][2] - temp[2][1] * temp[1][2]) * invertVec;
            vec[0][1] = (temp[0][2] * temp[2][1] - temp[0][1] * temp[2][2]) * invertVec;
            vec[0][2] = (temp[0][1] * temp[1][2] - temp[0][2] * temp[1][1]) * invertVec;
            vec[1][0] = (temp[1][2] * temp[2][0] - temp[1][0] * temp[2][2]) * invertVec;
            vec[1][1] = (temp[0][0] * temp[2][2] - temp[0][2] * temp[2][0]) * invertVec;
            vec[1][2] = (temp[1][0] * temp[0][2] - temp[0][0] * temp[1][2]) * invertVec;
            vec[2][0] = (temp[1][0] * temp[2][1] - temp[2][0] * temp[1][1]) * invertVec;
            vec[2][1] = (temp[2][0] * temp[0][1] - temp[0][0] * temp[2][1]) * invertVec;
            vec[2][2] = (temp[0][0] * temp[1][1] - temp[1][0] * temp[0][1]) * invertVec;
            return true;
        }
        else{
            return false;
        }
    }

    template <class T>
    const vector<vector<T>>& Matrix<T>::get_vec()const{
        return vec;
    }

    template <class T>
    Matrix<T>& Matrix<T>::operator=(const Matrix<T> &mat){
        if (this != &mat){
            vector<vector<T>> aVec = mat.get_vec();
            this->vec = aVec;
        }
        return *this;
    }

    template <class T>
    void Matrix<T>::print_matrix(){
        for (unsigned int i = 0; i < get_vec().size(); ++i){
            for (unsigned int j = 0; j < get_vec()[i].size(); ++j){
                std::cout << get_vec()[i][j] << ' ';
            }
            std::cout << endl;
        }
    }

    template <class T>
    vector<T> Matrix<T>::vector_multiply(vector<T> &vec){
        vector<T> multiplyVector = {0,0,0};
        for (size_t i = 0; i < get_vec().size(); ++i){
            T value = 0;
            for (size_t j = 0; j < get_vec().at(i).size(); ++j){
                value += vec.at(j) * get_vec().at(i).at(j);
            }
            multiplyVector.at(i) = value;
        }
        return multiplyVector;
    }


    template <class T>
    vector<vector<bool>> Matrix<T>::get_identity_matrix(){
        print_matrix();
        size_t dimension = get_vec().size();
        vector<vector<bool>> identity_mat(dimension, std::vector<bool> (dimension));
        for (unsigned int i = 0; i < get_vec().size(); ++i){
            for (unsigned int j = 0; j < get_vec()[i].size(); ++j){
                if (get_vec()[i][j] < 0.5){
                    identity_mat[i][j] = false;
                }
                else {
                    identity_mat[i][j] = true;
                }
            }
        }
        for (unsigned int i = 0; i < identity_mat.size(); ++i){
            for (unsigned int j = 0; j < identity_mat[i].size(); ++j){
                std::cout << identity_mat[i][j] << ' ';
            }
            std::cout << endl;
        }
        return identity_mat;
    }
} /* namespace std */
