/*
 * Matrix.hpp
 *
 *  Created on: Mar 17, 2018
 *      Author: Jimmy Feltsadas & Yoeri Smit
 */

#ifndef MATRIX_HPP_
#define MATRIX_HPP_

#include <vector>

namespace std {

    template<class T>

/**
 * @class This class holds a two dimensional vector of a given type and includes most of the basic operations.
 * It is a template class so that multiple types such as int, double, float and boolean can be used for the vector type.
 */
    class Matrix
    {

    public:
        explicit Matrix(const vector<vector<T>>& vec);
        virtual ~Matrix();
        const vector<vector<T>>& get_vec()const;
        Matrix<T> operator+(const Matrix<T> &mat);
        Matrix<T> operator-(const Matrix<T> &mat);
        Matrix<T> operator*(const Matrix<T> &mat);
        Matrix<T>& operator=(const Matrix<T> &mat);

        /**
         * @brief This function multiplies the current matrix with a vector.
         * @param vec The vector which the current matrix has to be multiplied with.
         * @return It returns a single rowed vector which is the result of the calculation.
         */
        vector<T> vector_multiply(vector <T> &vec);

        /**
         * @brief This function prints the entire content of the matrix to the console.
         */
        void print_matrix();

        /**
         * @brief This function executes a transpose operation on the matrix.
         */
        void transpose();

        /**
         * @brief This function inverses a three by three matrix.
         * @return It returns false if the determinant factor is 0.
         */
        bool inverse_three_tatrix();

        /**
         * @brief This function returns the identity version of the current matrix.
         * @return it returns a matrix which only holds 'zeros' and 'ones'. The 'ones' should be diagonal.
         */
        vector<vector<bool>> get_identity_matrix();

    private:
        vector<vector<T>> vec;
    };

} /* namespace std */

#endif /* MATRIX_HPP_ */
