//
// Created by jimmy on 17-3-18.
//

#include "Kinematics.hpp"
#include <iostream>
#include <opencv2/highgui.hpp>
#include <math.h>
#include "Config.hpp"
#include "Matrix.hpp"


/*static*/ cv::Point2d Kinematics::robot_base;
/*static*/ bool Kinematics::robot_base_Set = false;

Kinematics::Kinematics(const std::string& window_name, const double aScale) :   name(window_name), scale(aScale)
{

}

Kinematics::~Kinematics() {}

void Kinematics::initialise()
{
    scale_robot_dimensions(scale);
    // 0 = Shoulder
    // 1 = Elbow
    // 2 = Wrist-30, 120, 0
    configuration.push_back(0);
    configuration.push_back(50);
    configuration.push_back(0);

    beta = 0.00001;
}


std::vector<double> Kinematics::inverse_kinematics()
{
    // Obtained from Detector.
    S_Point target;
    target.x = T_dist;
    target.y = 0;

    // End effector a.k.a end point of robot.
    S_Point end_effector = forward_kinematics(-300,600, configuration[0], r_lower_arm, configuration[1], r_upper_arm, configuration[2], r_wrist, true);

    // Vector containing robot configuration angles
    std::vector<double> conf_rad = deg_to_rad(configuration);

    double marge = 0.00001;
    std::vector<double> deg_conf;
    while (goal_in_range(end_effector, target, marge) == false)
    {
        std::vector<std::vector<double>> jacobian = compute_jacobian(conf_rad);

        std::Matrix<double> inverseJacobian(jacobian);

        inverseJacobian.transpose();
        std::vector<double> delta_pos = calculate_delta_config(target, end_effector);

        std::vector<double> delta_theta = inverseJacobian.vector_multiply(delta_pos);

        conf_rad = calculate_new_angles(conf_rad, delta_theta);

        end_effector = forward_kinematics(0, 60, conf_rad[0], r_lower_arm, conf_rad[1], r_upper_arm, conf_rad[2], r_wrist, false);

        deg_conf = rad_to_deg(conf_rad);
    }
    for (unsigned int i = 0; i < deg_conf.size(); ++i)
    {
        std::cout << "Angle[" << i << "]: " << deg_conf[i] << std::endl;
    }
    std::cout << "Done with IK"<< deg_conf.size() << std::endl << "---------------------------------------" << std::endl;
    return deg_conf;
}

std::vector<double> Kinematics::calculate_delta_config(const S_Point &goal, const S_Point &current_pose){
    std::vector<double> beta_pose = {0,0};
    beta_pose.at(0) = beta * (goal.x - current_pose.x);
    beta_pose.at(1) = beta * (goal.y - current_pose.y);
    return beta_pose;
}

std::vector<double> Kinematics::calculate_new_angles(const std::vector<double> &old_conf,
                                                     const std::vector<double> &delta_theta){
    std::vector<double> new_angle_conf;
    for (size_t i = 0; i < old_conf.size(); ++i)
    {
        double new_angle = old_conf.at(i) + delta_theta.at(i);
        if (new_angle > M_PI)
        {
            new_angle = std::fmod(new_angle, M_PI);
        }
        if (new_angle < (M_PI * -1))
        {
            new_angle = std::fmod(new_angle, M_PI);
            new_angle *= -1;
        }
        new_angle_conf.push_back(new_angle);
    }
    return (new_angle_conf);
}

bool Kinematics::goal_in_range(S_Point e, S_Point T, double margin)
{
    bool ret = false;
    if (e.x <= T.x + margin && e.x >= T.x - margin &&
        e.y <= T.y + margin && e.y >= T.y - margin/*&& e.x + T.x < marge && e.y + T.y < marge*/)
    {

        ret = true;
    }
    return ret;

}

std::vector<double> Kinematics::deg_to_rad(const std::vector<double>& degrees)
{
    std::vector<double>radians;
    for (size_t i = 0; i < degrees.size(); ++i)
    {
        radians.push_back((degrees.at(i) / 180) * M_PI);
    }
    return radians;
}

std::vector<double> Kinematics::rad_to_deg(const std::vector<double>& radians){
    std::vector<double> degrees;
    for (size_t i = 0; i < radians.size(); ++i)
    {
        degrees.push_back(radians.at(i) / M_PI * 180);
    }
    return degrees;
}

/*static*/void Kinematics::onMouseStatic(int event, int x, int y, int flags, void* param)
{
    (void)flags; //Needed to remove unused warning
    Kinematics* ik = reinterpret_cast<Kinematics*>(param);
    ik->onMouse(event, x, y);
}


void Kinematics::onMouse(int event, int x, int y)
{
    if  (event == CV_EVENT_LBUTTONUP)
    {
        Kinematics::robot_base.x = x ;
        Kinematics::robot_base.y = y ;
        std::cout << "Point base selected: x:"<< robot_base.x << " y: " << robot_base.y << std::endl;

        Kinematics::robot_base_Set = true;
    }
}

void Kinematics::scale_robot_dimensions(double scale)
{
    r_lower_arm = L_LOWER_ARM * scale;
    r_upper_arm = L_UPPER_ARM * scale;
    r_wrist = L_WRIST * scale;
}


void Kinematics::set_robot_base()
{
    std::cout << "Set robot base:" << std::endl;
    cv::setMouseCallback(name, Kinematics::onMouseStatic, this);
}

double Kinematics::get_angle_base_to_obj(S_Object obj)
{
    double adjacent = std::abs(robot_base.x - obj.point.x);
    double opposite = std::abs(robot_base.y - obj.point.y);
    double hypotenuse = std::sqrt(std::pow(adjacent,2) + std::pow(opposite,2));

    T_dist = hypotenuse;
    double angle_deg = std::round(std::acos(adjacent / hypotenuse) * (180.0 / M_PI));

    if (robot_base.x > obj.point.x){
        angle_deg = (90 - angle_deg);
    }
    if (robot_base.x < obj.point.x)
    {
        angle_deg = angle_deg - 90;
    }

    if (robot_base.y > obj.point.y){
        angle_deg *= -1;
    }

    return angle_deg;
}

const cv::Point Kinematics::get_robot_base()
{
    return robot_base;
}

bool Kinematics::is_robot_base_set()
{
    return robot_base_Set;
}

;
S_Point Kinematics::forward_kinematics(double x0, double y0, double theta1, double l1, double theta2, double l2, double theta3,
                                     double l3, bool deg)
{

    if (deg)
    {
        theta1 = theta1 * M_PI / 180;
        theta2 = theta2 * M_PI / 180;
        theta3 = theta3 * M_PI / 180;
    }
    double x = x0 + ((l1 *sin(theta1)) + (l2 * sin(theta1 + theta2)) + (l3 * sin(theta1 + theta2 + theta3)));
    double y = y0 + ((l1 *cos(theta1)) + (l2 * cos(theta1 + theta2)) + (l3 * cos(theta1 + theta2 + theta3)));

    S_Point ret_point;
    ret_point.x = x;
    ret_point.y = y;

    return ret_point;
}

std::vector<std::vector<double>> Kinematics::compute_jacobian(const std::vector<double> &configuration)
{
    std::vector<std::vector<double>> jacobian = {{0,0,0},{0,0,0}};

    double second_angle_x = std::cos((configuration.at(0) + configuration.at(1)));
    double third_angle_x = std::cos((configuration.at(0) + configuration.at(1) + configuration.at(2)));

    jacobian.at(0).at(0) = std::cos(configuration.at(0)) + second_angle_x + third_angle_x;
    jacobian.at(0).at(1) = second_angle_x + third_angle_x;
    jacobian.at(0).at(2) = third_angle_x;

    double second_angle_y = -std::sin(configuration.at(0) + configuration.at(1));
    double third_angle_y = -std::sin(configuration.at(0) + configuration.at(1) + configuration.at(2));

    jacobian.at(1).at(0) = -std::sin(configuration.at(0)) + second_angle_y + third_angle_y;
    jacobian.at(1).at(1) = second_angle_y + third_angle_y;
    jacobian.at(1).at(2) = third_angle_y;

    return jacobian;
}

const std::vector<int> &Kinematics::get_robot_pos() const
{
    return robot_pos;
}

double Kinematics::get_lower_arm() const
{
    return r_lower_arm;
}

double Kinematics::get_upper_arm() const
{
    return r_upper_arm;
}

double Kinematics::get_wrist() const
{
    return r_wrist;
}

const std::vector<double> &Kinematics::get_configuration() const
{
    return configuration;
}

void Kinematics::set_scale(double scale)
{
    Kinematics::scale = scale;
}





