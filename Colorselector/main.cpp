/*#include "opencv2/opencv.hpp"
#include <iostream>
#include <cstring>

using namespace cv;
using namespace std;
// global variable to keep track of
bool show = false;

int LMin, aMin, bMin;
int LMax, aMax, bMax;
Scalar minLab, maxLab;


Mat imageBGR, imageLab;
Mat maskBGR, maskLab;
Mat resultBGR, resultLab;

// Create a callback for event on trackbars
void onTrackbarActivity(int pos, void* userdata){
    // Just uodate the global variable that there is an event
    show = true;
    return;
}

int main() {

    char* filename = "/home/yoeri/Projects/robotapplicatie/Jimmy_Yoeri_Robots/Images/plaatje1.png";
    Mat original = imread(filename);

    // image resize width and height
    int resizeHeight = 250;
    int resizeWidth = 250;
    //Size rsize(resizeHeight,resizeWidth);
    //resize(original, original, rsize);

    namedWindow("SelectLAB", WINDOW_AUTOSIZE);

    // creating trackbars to get values for LAB
    createTrackbar("LMin", "SelectLAB", 0, 255, onTrackbarActivity);
    createTrackbar("LMax", "SelectLAB", 0, 255, onTrackbarActivity);
    createTrackbar("AMin", "SelectLAB", 0, 255, onTrackbarActivity);
    createTrackbar("AMax", "SelectLAB", 0, 255, onTrackbarActivity);
    createTrackbar("BMin", "SelectLAB", 0, 255, onTrackbarActivity);
    createTrackbar("BMax", "SelectLAB", 0, 255, onTrackbarActivity);

    imshow("SelectLAB", original);



    char k;
    while (1) {

        if (show) { //If there is any event on the trackbar
            show = false;

            // Get values from the LAB trackbar
            LMin = getTrackbarPos("LMin", "SelectLAB");
            aMin = getTrackbarPos("AMin", "SelectLAB");
            bMin = getTrackbarPos("BMin", "SelectLAB");

            LMax = getTrackbarPos("LMax", "SelectLAB");
            aMax = getTrackbarPos("AMax", "SelectLAB");
            bMax = getTrackbarPos("BMax", "SelectLAB");

            minLab = Scalar(LMin, aMin, bMin);
            maxLab = Scalar(LMax, aMax, bMax);

            cvtColor(original, imageLab, COLOR_BGR2Lab);

            inRange(imageLab, minLab, maxLab, maskLab);
            resultLab = Mat::zeros(original.rows, original.cols, CV_8UC3);
            bitwise_and(original, original, resultLab, maskLab);

            // Show the results
            imshow("SelectLAB", resultLab);
        }
        waitKey(0);
    }
    destroyAllWindows();

    return 0;
}
 */

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>



cv::Mat lab;
int l_min = 0;
int l_max = 255;
int a_min = 0;
int a_max = 255;
int b_min = 0;
int b_max = 255;

void on_change(int, void *)
{
    cv::Mat lab_change(lab.size(), CV_8U);
    cv::Mat mask(lab.size(), CV_8U);

    cv::Scalar min(l_min, a_min, b_min);
    cv::Scalar max(l_max, a_max, b_max);

    cv::inRange(lab, min, max, mask);
    cv::bitwise_and(lab, lab, lab_change, mask);

    cv::imshow("lab", lab_change);
}

int main()
{

    cv::VideoCapture cap;
    // open the default camera, use something different from 0 otherwise;
    // Check VideoCapture documentation.
    if(!cap.open(0))
        return 0;

    cv::Mat frame;
    cap >> frame;

    //frame = cv::imread("/home/yoeri/Projects/robotapplicatie/Jimmy_Yoeri_Robots/Images/plaatje3.png");

                frame.copyTo(lab);
    cv::cvtColor(lab, lab, cv::COLOR_BGR2Lab);
    cv::namedWindow("lab", cv::WINDOW_FULLSCREEN);
    cv::createTrackbar("l_min", "lab", &l_min, 255, on_change);
    cv::createTrackbar("l_max", "lab", &l_max, 255, on_change);
    cv::createTrackbar("a_min", "lab", &a_min, 255, on_change);
    cv::createTrackbar("a_max", "lab", &a_max, 255, on_change);
    cv::createTrackbar("b_min", "lab", &b_min, 255, on_change);
    cv::createTrackbar("b_max", "lab", &b_max, 255, on_change);
    cv::imshow("lab", lab);
    cv::waitKey(0);

    cv::destroyAllWindows();
    return EXIT_SUCCESS;
}